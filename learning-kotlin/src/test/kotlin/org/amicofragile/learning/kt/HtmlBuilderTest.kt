package org.amicofragile.learning.kt

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class HtmlBuilderTest {
    private val EmptyHtml = """<html>
</html>
"""

    @Test
    fun emptyHtml() {
        val result = html {}
        assertThat(result, `is`(equalTo(EmptyHtml)))
    }

    @Test
    fun htmlWithTitle() {
        val result = html {
            head {
                add title "Hello, World!"
            }
        }
        assertThat(result, `is`(equalTo("""<html>
    <head>
        <title>Hello, World!</title>
    </head>
</html>
""")))
    }
}