package org.amicofragile.learning.kt

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.amicofragile.learning.kt.Distance.Companion.km
import org.amicofragile.learning.kt.Distance.Companion.m
import org.amicofragile.learning.kt.Distance.Companion.yd

class KLengthUnitiesTest {
    @Test
    fun valueInKm() {
        val distance = 88.9.km
        assertThat(distance.value, `is`(equalTo(88900.0)))
    }

    @Test
    fun valueInM() {
        val distance = 3.14.m
        assertThat(distance.value, `is`(equalTo(3.14)))
    }

    @Test
    fun valueInYd() {
        val distance = 2.0.yd
        assertThat(distance.value, `is`(equalTo(1.8288)))
    }

    @Test
    fun canSumDistances() {
        val distance = 88.9.km + 3.14.m + 2.0.yd
        assertThat(distance.value, `is`(equalTo(88900 + 3.14 + 1.8288)))
    }
}