package org.amicofragile.learning.kt.scripting

import org.amicofragile.learning.kt.Distance
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.hamcrest.CoreMatchers.*
import java.io.InputStream
import java.io.InputStreamReader
import java.io.StringReader
import javax.script.ScriptEngine
import javax.script.ScriptEngineManager

class ScriptingTest {
    private fun getEngine() : ScriptEngine {
        return ScriptEngineManager().getEngineByExtension("kts")
    }

    private fun loadResourceAsStream(name: String) : InputStream {
        return ClassForScripting::class.java.getResourceAsStream(name)
    }

    private fun loadResource(name: String) : String {
        return ClassForScripting::class.java.getResource(name).readText()
    }

    private fun loadUnitiesScript(name: String) : String {
        return ("""
import org.amicofragile.learning.kt.Distance.Companion.km
import org.amicofragile.learning.kt.Distance.Companion.m
import org.amicofragile.learning.kt.Distance.Companion.yd
        """ + loadResource(name)).trimIndent()
    }

    @Test
    fun script00() {
        val engine = getEngine()
        assertThat(engine, `is`(notNullValue()))
        val stream = loadResourceAsStream("/scripting/script00.kts")
        assertThat(stream, `is`(notNullValue()))
        val loaded : ClassForScripting = engine.eval(InputStreamReader(loadResourceAsStream("/scripting/script00.kts"))) as ClassForScripting
        assertThat(loaded.value, `is`(equalTo("First object from script!")))
    }

    @Test
    fun script01Dsl() {
        val engine = getEngine()
        val loaded : Distance = engine.eval(StringReader(loadResource("/scripting/script01.kts"))) as Distance
        assertThat(loaded.value, `is`(equalTo(88900 + 3.14 + 1.8288)))
    }

    @Test
    fun script01DslDefaultImports() {
        val engine = getEngine()
        val loaded : Distance = engine.eval(StringReader(loadUnitiesScript("/scripting/script02.kts"))) as Distance
        assertThat(loaded.value, `is`(equalTo(88900 + 3.14 + 1.8288)))
    }


}