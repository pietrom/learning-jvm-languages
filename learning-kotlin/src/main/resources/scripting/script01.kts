import org.amicofragile.learning.kt.Distance.Companion.km
import org.amicofragile.learning.kt.Distance.Companion.m
import org.amicofragile.learning.kt.Distance.Companion.yd

88.9.km + 3.14.m + 2.0.yd