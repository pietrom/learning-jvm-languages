package org.amicofragile.learning.kt

import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

suspend fun main() {
    val job = GlobalScope.launch {
//        doNotSoCooperativeJob()
//        doReallyCooperativeJob(context = coroutineContext)
        doReallyCooperativeAndIdiomaticJob()
    }
    println("Job started")
    delay(2000)
    job.cancel()

    println("Job canceled")
    delay(2000)
    println("Main completed")
}

fun doNotSoCooperativeJob() {
    while (true) {
        println("I'm running")
        Thread.sleep(300)
    }
}

fun doReallyCooperativeJob(context: CoroutineContext) {
    while (context.isActive) {
        println("I'm running")
        Thread.sleep(300)
    }
}

suspend fun doReallyCooperativeAndIdiomaticJob() {
    println("I'm running")
    Thread.sleep(300)
    yield()
}