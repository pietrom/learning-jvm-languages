package org.amicofragile.learning.kt.org.amicofragile.khttp

import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder
import java.time.ZonedDateTime
import java.io.BufferedReader
import java.io.InputStreamReader


//const val BaseUrl = "https://postb.in/1561459076024-9664884344674"
//const val BaseUrl = "https://donaldduck.free.beeceptor.com"
const val BaseUrl = "https://webhook.site/a38247bd-ca2c-4236-8ac6-d4daae7072b0"
const val ApiUrl = "api/v1/sample"

fun main(args: Array<String>) {
//    val url = URL("$BaseUrl/$ApiUrl")
//    val connection: HttpURLConnection = url.openConnection() as HttpURLConnection
//    connection.setRequestProperty("X-foo", "bar")
//    connection.requestMethod = "POST"
//    connection.connect()
//    val responseCode = connection.responseCode
//    println(responseCode)
//    println(connection.responseMessage)

    withBaseUrl(BaseUrl) {
        header(id = "X-PoweredBy", value = "KHttp")
        header(id = "X-ShouldBeReplaced", value = "Common value")
        authentication bearer "AABBCCDD"

        val pResult = post(ApiUrl) {
            header(id = "X-foo", value = "foo bar buzz")
            header(id = "X-ShouldBeReplaced", value = "POST replacement value")
        }
        println("POST result: $pResult")

        val gResult = get(ApiUrl) {
            header(id = "X-foo", value = "foo bar buzz get")
            queryString {
                param(name = "x", value = 123)
                param(name = "y", value = "abc")
                param(name = "z", value = ZonedDateTime.now().toOffsetDateTime())
            }
        }
        println("GET result: $gResult")
    }
}

@DslMarker
annotation class KHttpTagMarker

fun withBaseUrl(baseUrl: String, init: KHttp.() -> Unit) : Unit {
    val kHttp = KHttp(baseUrl)
    kHttp.init()
}

@KHttpTagMarker
class KHttp(val baseUrl: String) {
    val authentication = this
    private val commonHeaders = mutableMapOf<String, String>()

    fun post(url: String, init: KHttpRequest.() -> Unit) : KHttpResponse {
        val request = KHttpRequest("$baseUrl/$url", "POST", commonHeaders)
        request.init()
        return request.response
    }

    fun get(url: String, init: KHttpRequest.() -> Unit) : KHttpResponse {
        val request = KHttpRequest("$baseUrl/$url", "GET", commonHeaders)
        request.init()
        return request.response
    }

    fun header(id: String, value: String) {
        commonHeaders[id] = value
    }

    infix fun bearer(token: String) {
        header("Authentication", "Bearer: $token")
    }
}

@KHttpTagMarker
class KHttpRequest(val url: String, val method: String, val commonHeaders: Map<String, String>) {
    private val headers = mutableMapOf<String, String>()
    private val queryString = KQueryString()

    fun header(id: String, value: String) {
        headers[id] = value
    }

    fun queryString(init: KQueryString.() -> Unit) {
        queryString.init()
    }

    val response : KHttpResponse by lazy {
        val theUrl = URL("$url?$queryString")
        val connection: HttpURLConnection = theUrl.openConnection() as HttpURLConnection
        for (h in mergeHeaders()) {
            connection.setRequestProperty(h.key, h.value)
        }
        connection.requestMethod = method
        connection.connect()
        KHttpResponse(connection.responseCode, connection.responseMessage, readResponseBody(connection))
    }

    private fun mergeHeaders() : Map<String, String> {
        val result = mutableMapOf<String, String>()
        result.putAll(commonHeaders)
        result.putAll(headers)
        return result
    }

    private fun readResponseBody(connection: HttpURLConnection): String {
        val br = BufferedReader(InputStreamReader(connection.inputStream))
        val sb = StringBuilder()
        var output: String?
        do {
            output = br.readLine()
            if(output != null) {
                sb.append(output)
            }
        } while(output != null)
        return sb.toString()
    }
}

@KHttpTagMarker
class KQueryString {
    private val parameters = mutableMapOf<String, Any>()

    fun param(name: String, value: Any) {
        parameters[name] = value
    }

    override fun toString(): String {
        return parameters.entries.fold("") {acc, curr ->
            val param = URLEncoder.encode("${curr.key}=${curr.value}", "UTF-8")
            "$param&$acc"
        }
    }
}

data class KHttpResponse(val code: Int, val message: String, val body: String = "")