package org.amicofragile.learning.kt

data class MMoney(val amount: Int, val currency: String)

operator fun Int.invoke(currency: String) = MMoney(this, currency)


fun main() {
    useMoney {
        val m = amount EUR 100
        println(m)
    }
}

fun useMoney(init: MoneyContext.() -> Unit) {
    val ctx = MoneyContext()
    ctx.init()
}

class MoneyContext {
    val amount = this

    infix fun EUR(amount: Int) = MMoney(amount, "EUR")
}