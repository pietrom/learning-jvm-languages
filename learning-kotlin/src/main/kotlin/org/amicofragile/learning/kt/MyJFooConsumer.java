package org.amicofragile.learning.kt;

@FunctionalInterface
public interface MyJFooConsumer {
    void consume(Foo foo);
}
