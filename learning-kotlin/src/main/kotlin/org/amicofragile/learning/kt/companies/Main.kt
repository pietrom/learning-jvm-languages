package org.amicofragile.learning.kt.companies

import org.amicofragile.learning.companies.Company
import org.amicofragile.learning.companies.Owner
import org.amicofragile.learning.companies.SocialAccounts
import org.amicofragile.learning.companies.builders.CompanyBuilder
import org.amicofragile.learning.companies.builders.OwnerBuilder
import org.amicofragile.learning.companies.builders.SocialAccountsBuilder

fun main(args: Array<String>) {
    val accountsBuilder = SocialAccountsBuilder()
    accountsBuilder.facebook = "amicofragile78"
    accountsBuilder.twitter = "@amicofragile78"
    accountsBuilder.instagram = null

    val owner0 = OwnerBuilder()
    owner0.firstName = "Pietro"
    owner0.lastName = "Martinelli"
    owner0.share = 0.50

    val owner1Accounts = SocialAccountsBuilder()
    owner1Accounts.instagram = "russocri"
    owner1Accounts.twitter = null
    owner1Accounts.facebook = "russocri"

    val owner1 = OwnerBuilder()
    owner1.firstName = "Cristina"
    owner1.lastName = "Russo"
    owner1.share = 0.50
    owner1.accounts = owner1Accounts.build()

    val builder = CompanyBuilder()
    builder.name = "Turtles"
    builder.accounts = accountsBuilder.build()
    builder.owners.add(owner0.build())
    builder.owners.add(owner1.build())

    val company = builder.build()

    println("Company: $company")

    val company2 = company {
        name = "Turtles 2"
        accounts {
            facebook = "amicofragile78"
            twitter = "@amicofragile78"
        }
        owners {
            owner {
                firstName = "Pietro"
                lastName = "Martinelli"
                share = 0.50
            }
            owner {
                firstName = "Cristina"
                lastName = "Russo"
                share = 0.50
                accounts {
                    instagram = "russocri"
                    facebook = "russocri"
                }

//                owner { // compilation error
//                    firstName = "Cristina2"
//                    lastName = "Russo2"
//                    share = 0.50
//                    accounts {
//                        instagram = "russocri"
//                        facebook = "russocri"
//                    }
//                }
            }
        }
    }

    println("Company2: $company2")
}

@DslMarker
annotation class CompanyDsl

@CompanyDsl
fun company(init: KCompanyBuilder.() -> Unit) : Company {
    val builder = KCompanyBuilder()
    builder.init()
    return builder.build()
}

@CompanyDsl
class KCompanyBuilder {
    var name: String? = null
    private var socialAccounts: SocialAccounts? = null
    private var owners = mutableListOf<Owner>()

    fun accounts(init: KSocialAccountsBuilder.() -> Unit) {
        val builder = KSocialAccountsBuilder()
        builder.init()
        socialAccounts = builder.build()
    }

    fun owners(init: KOwnersBuilder.() -> Unit) {
        val builder = KOwnersBuilder()
        builder.init()
        owners.addAll(builder.build())
    }

    fun build() : Company {
        return Company(name, owners, socialAccounts)
    }
}

@CompanyDsl
class KSocialAccountsBuilder {
    var twitter:String? = null
    var facebook:String? = null
    var instagram:String? = null

    fun build() : SocialAccounts {
        return SocialAccounts(twitter, facebook, instagram)
    }
}

@CompanyDsl
class KOwnersBuilder {
    private val list = mutableListOf<Owner>()

    fun build(): List<Owner> {
        return list
    }

    fun owner(init: KOwnerBuilder.() -> Unit) {
        val builder = KOwnerBuilder()
        builder.init()
        list.add(builder.build())
    }
}

@CompanyDsl
class KOwnerBuilder {
    var firstName : String? = null
    var lastName : String? = null
    var share : Double = 1.0
    var socialAccounts: SocialAccounts? = null

    fun accounts(init: KSocialAccountsBuilder.() -> Unit) {
        val builder = KSocialAccountsBuilder()
        builder.init()
        socialAccounts = builder.build()
    }

    fun build() : Owner {
        return Owner(firstName, lastName, share, socialAccounts)
    }
}