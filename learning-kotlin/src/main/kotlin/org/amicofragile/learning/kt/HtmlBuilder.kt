package org.amicofragile.learning.kt

import org.w3c.dom.Text
import java.lang.StringBuilder

fun html( init: Html.() -> Unit) : String {
    val html = Html()
    html.init()
    return html.toHtml()
}

interface HtmlNode {
    fun toHtml(indent: String = "") : String
}

open class HtmlNodeWithChildren(val tagName: String) : HtmlNode {
    private val SingleIndentation = "    "
    private val children: MutableList<HtmlNode> = mutableListOf()

    protected fun addChild(node: HtmlNode) {
        children.add(node)
    }

    override fun toHtml(indent: String): String {
        val bld = StringBuilder()
        bld.append("$indent<$tagName>\n")
        for (child in children) {
            bld.append(child.toHtml("$indent$SingleIndentation"))
        }
        bld.append("$indent</$tagName>\n")
        return bld.toString()
    }
}

class TextNode(val tagName: String, val text: String) : HtmlNode {
    override fun toHtml(indent: String): String {
        return "$indent<$tagName>$text</$tagName>\n"
    }

}

@DslMarker
annotation class HtmlTag

@HtmlTag
class Html : HtmlNodeWithChildren("html") {

    fun head(init: Head.() -> Unit) {
        val head = Head()
        head.init()
        addChild(head)
    }
}

@HtmlTag
class Head : HtmlNodeWithChildren("head") {
    val add = this

    infix fun title(title: String) {
        addChild(TextNode("title", title))
    }
}