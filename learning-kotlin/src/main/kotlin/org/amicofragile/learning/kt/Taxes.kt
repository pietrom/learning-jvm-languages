package org.amicofragile.learning.kt

import java.math.BigDecimal

enum class Country {
    Canada, Us, Uk, Finland, Israel
}

fun For(country: Country) = CountryContainer(country)

data class CountryContainer(val country: Country) {
    infix fun take(tax: BigDecimal): TaxAndCountry =
        TaxAndCountry(country, tax)

    infix fun take(tax: Double): TaxAndCountry = take(tax.toBigDecimal())
}

data class TaxAndCountry(override val country: Country, val tax: BigDecimal) : CountryCalculator {
    override val calculator: TaxCalculator
        get() = { cart -> cart.total * tax}

    infix fun ignore(comb: CartCombinator) : CountryToTaxCalculation =
        CountryToTaxCalculation(country) { cart: Cart -> (cart.total - comb.cartField(cart)) * tax}
}

class CartCombinator(val cartField: CartField) {
    infix fun and(comb: CartCombinator) : CartCombinator  =
        CartCombinator { c: Cart -> cartField(c) + comb.cartField(c) }

        fun If(cond: CartPredicate) : CartCombinator  =
        CartCombinator { c: Cart -> if(cond(c)) cartField(c) else BigDecimal.ZERO }

        infix fun greaterThan (value: BigDecimal) : CartPredicate =
            { c: Cart -> cartField(c) > value }

        infix fun greaterThan (value: Int) : CartPredicate = greaterThan(value.toBigDecimal())
}

data class Cart(val total:BigDecimal, val shipping: BigDecimal, val discount: BigDecimal, val country: Country)

typealias TaxCalculator = (c:Cart) -> BigDecimal
typealias CartField = (Cart) -> BigDecimal
typealias CartPredicate = (Cart) -> Boolean

interface CountryCalculator {
    val country : Country
    val calculator : TaxCalculator
}

data class CountryToTaxCalculation(override val country: Country, override val calculator: TaxCalculator) : CountryCalculator

val shipping = CartCombinator { cart:Cart -> cart.shipping }
val discount = CartCombinator { cart:Cart -> cart.discount }

fun main() {
    //    val sf2: CountryToTaxCalculation = For(Country.Sf) take 0.05 ignore (discount If shipping > 4)
//    val israel: CountryToTaxCalculation = For(Country.Israel) take 0.25 addTouristPrice { cart => cart.total * 0.2}
//
    val taxRules: List<CountryCalculator> = listOf(
            For(Country.Canada) take 0.15,
            For(Country.Uk) take 0.12 ignore shipping,
            For(Country.Us) take 0.07 ignore (shipping and discount),
            For(Country.Finland) take 0.05 ignore discount.If(shipping greaterThan 4)
    )
    calculateTax(taxRules, Cart(1000.toBigDecimal(), 100.toBigDecimal(), 20.toBigDecimal(), Country.Canada))
    calculateTax(taxRules, Cart(1000.toBigDecimal(), 100.toBigDecimal(), 20.toBigDecimal(), Country.Uk))
}

fun calculateTax(taxRules: List<CountryCalculator>, cart: Cart) {
    val taxRule: TaxCalculator? = taxRules findRuleFor cart
    println(taxRule?.invoke(cart))
}

infix fun List<CountryCalculator>.findRuleFor(cart : Cart) = this.find { x -> x.country == cart.country } ?.calculator