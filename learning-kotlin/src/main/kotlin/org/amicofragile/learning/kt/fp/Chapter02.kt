package org.amicofragile.learning.kt.fp

import org.amicofragile.learning.kt.fp.Chapter02_01.fibonacci
import org.amicofragile.learning.kt.fp.Chapter02_02.isSorted
import org.amicofragile.learning.kt.fp.Chapter02_03.curry
import org.amicofragile.learning.kt.fp.Chapter02_03.partial1
import org.amicofragile.learning.kt.fp.Chapter02_04.uncurry
import org.amicofragile.learning.kt.fp.Chapter02_05.compose

object Chapter02_01 {
    fun fibonacci(n: Int): Int {
        if (n == 0 || n == 1) return n
        tailrec fun loop(index: Int, prev: Int, prev2: Int): Int {
            return if (index == n) prev + prev2 else loop(index + 1, prev + prev2, prev)
        }
        return loop(2, 1, 0)
    }
}

object Chapter02_02 {
    fun <A> isSorted(list: List<A>, comparer: (x: A, y: A) -> Boolean): Boolean {
        tailrec fun loop(index: Int): Boolean {
            if (index >= list.size - 1) return true
            return if (!comparer(list[index], list[index + 1])) false else loop(index + 1)
        }
        return loop(0)
    }
}

object Chapter02_03 {
    fun <A, B, C> partial1(a: A, f: (A, B) -> C): (B) -> C = { b: B -> f(a, b) }

    fun <A, B, C> curry(f: (A, B) -> C): (A) -> (B) -> C = { a : A -> { b: B  -> f(a, b) }}
}

object Chapter02_04 {
    fun <A, B, C> uncurry(f: (A) -> ((B) -> C)) : (A, B) -> C = { a, b -> f(a)(b) }
}
object Chapter02_05 {
    fun <A, B, C> compose(f: (B) -> C, g: (A) -> B) : (A) -> C = { a -> f(g(a)) }
}


fun main() {
    for (i in 1..10) {
        println(fibonacci(i))
    }

    println(isSorted(listOf<Int>()) { x, y -> x < y })
    println(isSorted<Int>(listOf(1, 3, 5, 7, 9, 11, 13)) { x, y -> x < y })
    println(isSorted<String>(listOf("aa", "cc", "vv", "bb", "zz")) { x, y -> x < y })
    println(isSorted<String>(listOf("aa", "cc", "vv", "bb", "zz")) { x, y -> x > y })
    println(isSorted<String>(listOf("aa", "cc", "vv", "yy", "zz")) { x, y -> x < y })

    val adder: (Int, Int) -> Int = { x, y -> x + y }
    val add2 = partial1(2, adder)
    println(add2(11))
    println(add2(17))
    println(partial1(10, adder)(10))

    println(curry(adder)(2)(11))
    println(curry(adder)(2)(17))
    println(curry(adder)(10)(10))

    val partialAdder: (Int) -> (Int) -> Int = { x: Int ->  { y: Int  -> x + y }}
    val uncurriedAdder = uncurry(partialAdder)
    println(uncurriedAdder)
    println(uncurriedAdder(11, 19))

    val double : (Int) -> Int = { x: Int -> 2 * x }
    val format : (Int) -> String =  { x: Int -> "Double is ${x}" }
    val formatDouble = compose(format, double)
    println(formatDouble(11))
    println(formatDouble(19))
}