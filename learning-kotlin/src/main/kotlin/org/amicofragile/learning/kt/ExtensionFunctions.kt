package org.amicofragile.learning.kt

fun main() {
    val theMap = mapOf(1 arrow "Uno", 2 arrow "Due", 3 arrow "Tre")
    println(theMap)
}

infix fun <A, B> A.arrow(value: B) : Pair<A, B> {
    return Pair<A, B>(this, value)
}