package org.amicofragile.learning.kt

data class Distance private constructor(val value:Double) {
    operator fun plus(other: Distance): Distance {
        return Distance(this.value + other.value)
    }

    companion object {
        val Double.km : Distance
            get() = Distance(1000 * this)

        val Double.m : Distance
            get() = Distance(this)

        val Double.yd : Distance
            get() = Distance(0.9144 * this)
    }
}

