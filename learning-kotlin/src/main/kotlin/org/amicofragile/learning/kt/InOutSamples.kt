package org.amicofragile.learning.kt

class MyConsumer<in T> {
    fun foo(t: T) {}

    fun foo(f: () -> T) {}

    //fun foo(f: (t: T) -> Unit) {} // compilation error

    fun foo(f: (g: (T) -> Unit) -> Unit) {
    }

    //fun bar() : T {} // compilation error

    //fun bar() : () -> T {} // compilation error
    fun bar(input: T) : (T) -> Unit {
        return { x  -> input }
    }

    fun bar() : (f: () -> T) -> Unit {
        return { g -> g() }
    }
}