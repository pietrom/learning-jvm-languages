package org.amicofragile.learning.kt

import java.lang.StringBuilder

class Foo

interface MyFooConsumer {
    fun consume(foo: Foo)
}

fun applyConsumer(c: MyFooConsumer) {
    c.consume(Foo())
}

fun applyJConsumer(c: MyJFooConsumer) {
    c.consume(Foo())
}

fun applyJConsumer2(c: (Foo) -> Unit) {
    c(Foo())
}

fun run() {
    applyConsumer(object : MyFooConsumer {
        override fun consume(foo: Foo) {
            println(foo)
        }
    })

    applyJConsumer(object : MyJFooConsumer {
        override fun consume(foo: Foo?) {
            println(foo)
        }
    })

    applyJConsumer (MyJFooConsumer { foo -> println(foo) })

    applyJConsumer(MyJFooConsumer { println(it) })

    //applyConsumer(MyFooConsumer { println(it) }) // compilation error

    applyJConsumer2( { foo -> println(foo) })

    applyJConsumer2( { println(it) })

    applyJConsumer2 { println(it) }
}

fun <T, TOut> T.myApply(f : T.() -> TOut) : T {
    this.f()
    return this
}

fun main() {
    val sb = StringBuilder("Pietro")
    val res = sb.myApply {
        append(" ")
        append("Martinelli")
    }.toString()
    println(res)
}