package org.amicofragile.learning.kt

import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext
import kotlin.system.measureTimeMillis

//suspend fun main(args: Array<String>) {
//    println("Start")
//
//    // Start a coroutine
//    val job = GlobalScope.launch {
//        delay(3000)
//        println("... World!")
//    }
//    println("Hello, ...")
//    job.join()
//    println("Stop")
//}

suspend fun main (args: Array<String>) {
    println("Start main thread")
    val job = GlobalScope.launch {
        Jobs.longRunningProcess("0", 2000)
    }
    job.join()
    runBlocking {
        Jobs.longRunningProcess("1", 2000)
    }
    val elapsed = measureTimeMillis {
        val r1 = GlobalScope.async {
            delay(1000)
            5
        }
        val r2 = GlobalScope.async {
            delay(1500)
            37
        }
        val result = r1.await() + r2.await()
        val result2 = awaitAll(r1, r2).sum()
        println("Result is $result [$result2]")
    }
    println("Elapsed is $elapsed")
    coroutineScope {
        launch {
            println("Before...")
            delay(1000)
            println("... After!")
        }
    }
    println("Main thread exits")
}

object Jobs {
    suspend fun longRunningProcess(data: String, delay: Long) {
        println("Long process started... $data")
        delay(delay)
        println("Long process completed $data")
    }


}

