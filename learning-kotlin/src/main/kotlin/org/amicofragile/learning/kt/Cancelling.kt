package org.amicofragile.learning.kt

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

suspend fun main() {
    val job = GlobalScope.launch {
        doJob()
    }
    println("Job started")
    delay(2000)
    job.cancel()

    println("Job canceled")
    delay(2000)
    println("Main completed")
}

suspend fun doJob() {
    while(true) {
        delay(300)
        println("I'm still running")
    }
}