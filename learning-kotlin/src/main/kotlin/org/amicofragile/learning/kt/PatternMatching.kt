package org.amicofragile.learning.kt

fun applyPatternMatching(input: Parent) {
    when(input) {
        is Child1 -> println("Child1: ${input.x}, ${input.y}")
        is Child2 -> println("Child2: ${input.value}")
    }
    
    val result: String = when(input) {
        is Child1 -> if (input.x == 17) "17" else input.y
        is Child2 -> input.value
    }

    println("String result is $result")
}

fun main() {
    applyPatternMatching(Child1(11, "19"))
    applyPatternMatching(Child1(17, "22"))
    applyPatternMatching(Child2("xyz"))
}

sealed class Parent

data class Child1(val x: Int, val y: String) : Parent()

data class Child2(val value: String) : Parent()