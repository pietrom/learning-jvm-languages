package org.amicofragile.learning.companies.builders;

import org.amicofragile.learning.companies.Company;
import org.amicofragile.learning.companies.Owner;
import org.amicofragile.learning.companies.SocialAccounts;

import java.util.Collection;
import java.util.LinkedList;

public class CompanyBuilder {
    public String name;
    public SocialAccounts accounts;
    public Collection<Owner> owners = new LinkedList<>();

    public Company build() {
        return new Company(name, owners, accounts);
    }
}
