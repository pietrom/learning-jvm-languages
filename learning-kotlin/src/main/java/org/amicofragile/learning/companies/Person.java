package org.amicofragile.learning.companies;

public class Person {
    private final String firstName;
    private final String lastName;
    private final SocialAccounts socialAccounts;

    public Person(String firstName, String lastName, SocialAccounts socialAccounts) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialAccounts = socialAccounts;
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", accounts='" + socialAccounts + '\'' +
                '}';
    }
}
