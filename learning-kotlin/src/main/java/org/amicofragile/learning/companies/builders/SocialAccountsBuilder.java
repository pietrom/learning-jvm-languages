package org.amicofragile.learning.companies.builders;

import org.amicofragile.learning.companies.SocialAccounts;

public class SocialAccountsBuilder {
    public String facebook, twitter, instagram;

    public SocialAccounts build() {
        return new SocialAccounts(twitter, facebook, instagram);
    }
}
