package org.amicofragile.learning.companies;

import java.util.Collection;
import java.util.stream.Collectors;

public class Company {
    private final String name;
    private final Collection<Owner> owners;
    private final SocialAccounts socialAccounts;

    public Company(String name, Collection<Owner> owners, SocialAccounts socialAccounts) {
        this.name = name;
        this.owners = owners;
        this.socialAccounts = socialAccounts;
    }

    @Override
    public String toString() {
        return "Company{" +
                "name='" + name + '\'' +
                ", owners=" + owners.stream().map(o -> o.toString()).collect(Collectors.joining("|")) +
                ", accounts='" + socialAccounts + '\'' +
                '}';
    }
}
