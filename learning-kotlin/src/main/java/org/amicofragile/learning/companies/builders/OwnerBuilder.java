package org.amicofragile.learning.companies.builders;

import org.amicofragile.learning.companies.Owner;
import org.amicofragile.learning.companies.SocialAccounts;

public class OwnerBuilder {
    public String firstName, lastName;
    public Double share;
    public SocialAccounts accounts;

    public Owner build() {
        return new Owner(firstName, lastName, share, accounts);
    }
}
