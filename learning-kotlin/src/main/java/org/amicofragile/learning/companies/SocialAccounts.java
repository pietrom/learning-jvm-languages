package org.amicofragile.learning.companies;

public class SocialAccounts {
    private String twitter;
    private String facebook;
    private String instagram;

    public SocialAccounts(String twitter, String facebook, String instagram) {
        this.twitter = twitter;
        this.facebook = facebook;
        this.instagram = instagram;
    }

    @Override
    public String toString() {
        return "SocialAccounts{" +
                "twitter='" + twitter + '\'' +
                ", facebook='" + facebook + '\'' +
                ", instagram='" + instagram + '\'' +
                '}';
    }
}
