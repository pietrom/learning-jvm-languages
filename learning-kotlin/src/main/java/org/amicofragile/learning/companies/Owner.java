package org.amicofragile.learning.companies;

public class Owner extends Person {
    private final Double share;
    public Owner(String firstName, String lastName, Double share, SocialAccounts socialAccounts) {
        super(firstName, lastName, socialAccounts);
        this.share = share;
    }

    @Override
    public String toString() {
        return "Owner{" + super.toString() +
                ",share=" + share +
                '}';
    }
}
