package org.amicofragile.learning.groovy

import static org.hamcrest.CoreMatchers.*
import static org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class GLengthUnitiesTest {
    private def initExpr(Closure action) {
        use(GLengthUnities) {
            return action()
        }
    }
    @Test
    void valueInKm() {
        def distance = initExpr {
            88.9.km()
        }
        assertThat(distance.value, is(equalTo(88900.0)))
    }

    @Test
    void valueInM() {
        def distance = initExpr {
            3.14.m()
        }
        assertThat(distance.value, is(equalTo(3.14)))
    }

    @Test
    void valueInYd() {
        def distance = initExpr {
            2.0.yd()
        }
        assertThat(distance.value, is(equalTo(1.82880)))
    }

    @Test
    void canSumDistances() {
        def distance = initExpr {
            88.9.km() + 3.14.m() + 2.0.yd()
        }
        assertThat(distance.value, is(equalTo(88900 + 3.14 + 1.82880)))
    }
}

