package org.amicofragile.learning.groovy

import static org.hamcrest.CoreMatchers.*
import static org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class GHelloTest {
    @Test
    void shouldSayHelloWorld() {
        assertThat(new GHello().sayHello(), is(equalTo("Hello, World!")))
    }
}
