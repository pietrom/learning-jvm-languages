package org.amicofragile.learning.groovy

class Distance {
    BigDecimal value

    Distance(BigDecimal value) {
        this.value = value
    }

    Distance plus(Distance other) {
        new Distance(this.value.plus(other.value))
    }
}

class GLengthUnities {
    static Distance km(BigDecimal value) {
        return new Distance(1000 * value)
    }

    static Distance m(BigDecimal value) {
        return new Distance(value)
    }

    static Distance yd(BigDecimal value) {
        return new Distance(0.9144 * value)
    }
}
