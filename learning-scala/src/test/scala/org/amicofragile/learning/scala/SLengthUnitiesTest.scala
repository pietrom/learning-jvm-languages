package org.amicofragile.learning.scala

import org.hamcrest.CoreMatchers._
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import LengthUnitiesConversionHelper.LengthUnitiesConversion
import org.junit.Test


class SLengthUnitiesTest {

  implicit class IntConversion(val x: Int) {
    def square(): Int = x * x
  }

  @Test
  def valueInKm(): Unit = {
    val distance = 88.9.km
    assertThat(distance.value, `is`(equalTo(88900.0)))
  }

  @Test
  def valueInM(): Unit = {
    val distance = 3.14.m
    assertThat(distance.value, `is`(equalTo(3.14)))
  }

  @Test
  def valueInYd(): Unit = {
    val distance = 2.0.yd
    assertThat(distance.value, `is`(equalTo(1.8288)))
  }

  @Test
  def canSumDistances(): Unit = {
    val distance = 88.9.km + 3.14.m + 2.0.yd
    assertThat(distance.value, `is`(equalTo(88900 + 3.14 + 1.8288)))
  }

  @Test
  def canSquareIntegers() : Unit = {
    val result = 19.square()
    assertThat(result, is(equalTo(361)))
  }
}
