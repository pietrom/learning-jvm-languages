package org.amicofragile.learning.scala.gameoflife
import org.junit.Test
import org.hamcrest.CoreMatchers._
import org.hamcrest.MatcherAssert.assertThat
class GridTest {
  @Test
  def renderDeadGrid() = {
    val grid = new Grid(Array(
      Array(Dead(), Dead(), Dead(), Dead()),
      Array(Dead(), Dead(), Dead(), Dead()),
      Array(Dead(), Dead(), Dead(), Dead()),
      Array(Dead(), Dead(), Dead(), Dead()),
      Array(Dead(), Dead(), Dead(), Dead())
    ))
    assertThat(grid.render(), is(equalTo(
      """|----
        |----
        |----
        |----
        |----
        |""".stripMargin.replace(System.getProperty("line.separator"), "\n"))))
  }

  @Test
  def renderGrid() = {
    val grid = new Grid(Array(
      Array(Dead(), Alive(), Dead(), Dead()),
      Array(Alive(), Dead(), Alive(), Dead()),
      Array(Dead(), Alive(), Dead(), Dead()),
      Array(Alive(), Dead(), Alive(), Dead()),
      Array(Dead(), Alive(), Dead(), Dead())
    ))
    assertThat(grid.render(), is(equalTo(
      """|-O--
         |O-O-
         |-O--
         |O-O-
         |-O--
         |""".stripMargin.replace(System.getProperty("line.separator"), "\n"))))
  }

//  @Test
//  def singleAliveCellShouldDie() : Unit = {
//    val grid0 = new Grid(Array(Array(Alive())))
//    val grid1 = grid0.evolve()
//    val cell:Cell = grid1.cells(0)(0)
//    assertThat("Is Dead", cell.isInstanceOf[Dead])
//  }
}
