package org.amicofragile.learning.scala.fp

import org.amicofragile.learning.scala.fp.Chapter03_Tree.Tree._
import org.amicofragile.learning.scala.fp.Chapter03_Tree.{Leaf, Tree, Branch}
import org.junit.runner.RunWith
import org.scalatest.{FlatSpec, Matchers}
import org.scalatestplus.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class Chapter03_TreeSpec extends FlatSpec with Matchers {
  /*

  B -- B -- 22
         -- B -- 11
              -- 19

    -- B -- B -- 6
              -- 21
          -- 17

   */
  val tree = Branch(
    Branch(
      Leaf(22),
      Branch(
        Leaf(17), Leaf(19)
      )
    ),
    Branch(
      Branch(
        Leaf(6), Leaf(21)
      ),
      Leaf(17)
    )
  )

  "size" should "be 1 for single-leaf tree" in {
    Tree.size(Leaf(19)) should be(1)
  }

  it should "be 3 for single-branch tree with two leaves" in {
    Tree.size(Branch(Leaf(11) , Leaf(19))) should be(3)
  }

  it should "be calculated properly, considering subtrees' lengths" in {
    Tree.size(tree) should be (11)
  }

  "maximum" should "return leaf's value when tree contains a single Leaf" in {
    maximum(Leaf(19)) should be(19)
  }

  it should "traverse the Tree" in {
    maximum(tree) should be(22)
  }
}
