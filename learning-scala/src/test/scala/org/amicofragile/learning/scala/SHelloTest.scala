package org.amicofragile.learning.scala

import org.junit.Test
import org.hamcrest.CoreMatchers._
import org.hamcrest.MatcherAssert.assertThat

class SHelloTest {
  @Test
  def shouldSayHelloWorld() = {
    val hello = Hello()
    assertThat(hello.sayHello(), is(equalTo("Hello, World!")))
  }
}
