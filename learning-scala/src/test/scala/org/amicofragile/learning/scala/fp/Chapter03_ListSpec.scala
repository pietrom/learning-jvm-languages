package org.amicofragile.learning.scala.fp

import org.scalatest.{FlatSpec, Matchers}
import org.amicofragile.learning.scala.fp.Chapter03_List.List
import org.amicofragile.learning.scala.fp.Chapter03_List.Nil
import org.amicofragile.learning.scala.fp.Chapter03_List.List._
import org.junit.runner.RunWith
import org.scalatestplus.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class Chapter03_ListSpec extends FlatSpec with Matchers {
  "An empty list" should "have sum 0" in {
    sum(List()) should be(0)
  }

  it should "have product 1" in {
    prod(List()) should be(1)
  }

  it should "have None head" in {
    head(List()) should be(None)
  }

  it should "have Nil tail" in {
    tail(List()) should be(Nil)
  }

  "A not empty list of integers" should "have sum calculated properly" in {
    sum(List(1, 2, 3, 4, 5, 6)) should be(21)
  }

  it should "have product calculated properly" in {
    prod(List(1, 2, 3, 4, 5, 6)) should be(720)
  }

  "List.drop" should "work properly with empty list" in {
    drop(List(), 4) should be(List())
  }

  it should "work properly when index is lesser than list size" in {
    drop(List(1, 2, 3, 4, 5, 6), 4) should be(List(5, 6))
  }

  it should "work properly when index is equals to list size" in {
    drop(List(1, 2, 3, 4, 5, 6), 6) should be(List())
  }

  it should "work properly when index is greater than list size" in {
    drop(List(1, 2, 3, 4, 5, 6), 10) should be(List())
  }

  "List.dropWhile" should "work properly with empty list" in {
    dropWhile(List[Int]())(_ < 4) should be(List())
  }

  it should "work properly when predicate is false for one item" in {
    dropWhile(List(1, 2, 3, 4, 5, 6))(_ < 4) should be(List(4, 5, 6))
  }

  it should "work properly when index is true for each item" in {
    dropWhile(List(1, 2, 3, 4, 5, 6))(_ < 10) should be(List())
  }

  it should "work properly when index is false for each item" in {
    dropWhile(List(1, 2, 3, 4, 5, 6))(_ > 10) should be(List(1, 2, 3, 4, 5, 6))
  }

  "foldRight" should "associate RTL" in {
    foldRight(List(1, 2, 3, 4), 0)((curr, acc) => curr + 2 * acc) should be(49)
  }

  it should "permit to sum integers" in {
    foldRight(List(1, 2, 3, 4, 5, 6), 0)(_ + _) should be(21)
  }

  "foldRight on foldLeft" should "associate RTL" in {
    foldRight_fl(List(1, 2, 3, 4), 0)((curr, acc) => curr + 2 * acc) should be(49)
  }

  "foldRight on foldLeft" should "associate RTL with - operation" in {
    foldRight_fl(List(1, 2, 3, 4), 0)(_ - _) should be(-2)
  }

  "foldLeft" should "associate LTR" in {
    foldLeft(List(1, 2, 3, 4), 0)((acc, curr) => curr + 2 * acc) should be(26)
  }

  it should "permit to sum integers" in {
    foldLeft(List(1, 2, 3, 4, 5, 6), 0)(_ + _) should be(21)
  }

  "foldLeft on foldRight" should "associate LTR with commutative f" in {
    foldLeft_fr(List(1, 2, 3, 4), 0)((acc, curr) => curr + 2 * acc) should be(26)
  }

  it should "associate LTR with not commutative f" in {
    foldLeft_fr(List(1, 2, 3, 4), 0)((acc, curr) => acc - curr) should be(-10)
  }

  "setHead" should "work on empty List" in {
    setHead(List())(19) should be(List(19))
  }

  it should "work on singleton List" in {
    setHead(List(100))(19) should be(List(19))
  }

  it should "work on multi-value List" in {
    setHead(List(100, 200, 300))(19) should be(List(19, 200, 300))
  }

  "append" should "work when both Lists are empty" in {
    append(List[Int](), List[Int]()) should be(List[Int]())
  }

  it should "work when left List is empty" in {
    append(List[Int](), List(1, 2, 3)) should be(List(1, 2, 3))
  }

  it should "work when right List is empty" in {
    append(List(1, 2, 3), List()) should be(List(1, 2, 3))
  }

  it should "work when both List are not empty" in {
    append(List(1, 2, 3), List(4, 5, 6, 7)) should be(List(1, 2, 3, 4, 5, 6, 7))
  }

  "length" should "be 0 for empty List" in {
    List.length(List[Int]()) should be(0)
  }

  it should "be 1 for singleton List" in {
    List.length(List(11)) should be(1)
  }

  it should "work properly for multi-item List" in {
    List.length(List(11, 17, 19, 22, 6, 21)) should be(6)
  }

  "length_foldRight" should "be 0 for empty List" in {
    length_fr(List[Int]()) should be(0)
  }

  it should "be 1 for singleton List" in {
    length_fr(List(11)) should be(1)
  }

  it should "work properly for multi-item List" in {
    length_fr(List(11, 17, 19, 22, 6, 21)) should be(6)
  }

  "length_foldLeft" should "be 0 for empty List" in {
    length_fl(List[Int]()) should be(0)
  }

  it should "be 1 for singleton List" in {
    length_fl(List(11)) should be(1)
  }

  it should "work properly for multi-item List" in {
    length_fl(List(11, 17, 19, 22, 6, 21)) should be(6)
  }

  "reverse" should "be an empty List for an empty List()" in {
    reverse(List()) should be(List())
  }

  it should "be the same as the input list for singleton list" in {
    reverse(List("foo bar")) should be (List("foo bar"))
  }

  it should "work properly for multi-item List" in {
    reverse(List('a', 'b', 'c')) should be(List('c', 'b', 'a'))
  }

  "reverse on foldLeft" should "be an empty List for an empty List()" in {
    reverse_fl(List()) should be(List())
  }

  it should "be the same as the input list for singleton list" in {
    reverse_fl(List("foo bar")) should be (List("foo bar"))
  }

  it should "work properly for multi-item List" in {
    reverse_fl(List('a', 'b', 'c')) should be(List('c', 'b', 'a'))
  }

  "reverse on foldRight" should "be an empty List for an empty List()" in {
    reverse_fr(List()) should be(List())
  }

  it should "be the same as the input list for singleton list" in {
    reverse_fr(List("foo bar")) should be (List("foo bar"))
  }

  it should "work properly for multi-item List" in {
    reverse_fr(List('a', 'b', 'c')) should be(List('c', 'b', 'a'))
  }

  "flatten" should "map an empty List to an empty List" in {
    flatten(List[List[Int]]()) should be (List[List[Int]]())
  }

  it should "map a list containing a single list into a flatten List" in {
    flatten(List(List(11, 17, 19, 22))) should be (List(11, 17, 19, 22))
  }

  it should "concatenate nested Lists flattening them to a single list" in {
    flatten(List(List(1, 2, 3), List(4, 5, 6), List(7, 8, 9))) should be(
      List(1, 2, 3, 4, 5, 6, 7, 8, 9)
    )
  }

  it should "work with nested Lists of different types" in {
    flatten(List(List(1, 2), List("a", "b"), List(true, false, true))) should be (
      List(1, 2, "a", "b", true, false, true)
    )
  }

  "map" should "map empty List into empty List" in {
    map(List[String]())(_.length) should be (List[Int]())
  }

  it should "apply the provided function to each item of the input List" in {
    map(List("a", "bb", "ccc", "dddd"))(_.length) should be(List(1, 2, 3, 4))
  }

  "map_fl" should "map empty List into empty List" in {
    map_fl(List[String]())(_.length) should be (List[Int]())
  }

  it should "apply the provided function to each item of the input List" in {
    map_fl(List("a", "bb", "ccc", "dddd"))(_.length) should be(List(1, 2, 3, 4))
  }

  "flatMap" should "map empty List into empty List" in {
    flatMap(List[String]())((s) => List(s.toCharArray():_*)) should be (List[Char]())
  }

  "flatMap" should "apply provided function to each element of the List and flatten the resulting list" in {
    flatMap(List("Cristina", "Irene", "Laura"))((s) => List(s.toCharArray():_*)) should be (
      List[Char]("CristinaIreneLaura".toCharArray:_*))
  }

  "filter" should "map empty List into empty List" in {
    filter(List[Int]())(_ > 0) should be (List[Int]())
  }

  it should "exclude items for which the predicate is false" in {
    filter(List(1, 2, 3, 4, 5, 6, 7))(_ % 2 != 0) should be (List(1, 3, 5, 7))
  }

  "filter on flatMap" should "map empty List into empty List" in {
    filter_fm(List[Int]())(_ > 0) should be (List[Int]())
  }

  it should "exclude items for which the predicate is false" in {
    filter_fm(List(1, 2, 3, 4, 5, 6, 7))(_ % 2 != 0) should be (List(1, 3, 5, 7))
  }

  "mapZipped" should "map emtpy Lists into empty List" in {
    mapZipped(List[Int](), List[Int]())(_ + _) should be(List[Int]())
  }

  "mapZipped" should "apply mapping function to each tuple2" in {
    mapZipped(List(1, 2, 3), List('a', 'b', 'c'))(_ + _.toString) should be(List("1a", "2b", "3c"))
  }

  "take" should "map emptyList into empty List" in {
    take(List[Int](), 5) should be(List[Int]())
  }

  it should "return first n elements of the List" in {
    take(List(1, 2, 3, 4, 5), 3) should be(List(1, 2, 3))
  }

  it should "work even when List length is lesser than provided cutoff index" in {
    take(List(1, 2, 3, 4, 5), 10) should be(List(1, 2, 3, 4, 5))
  }

  "takeWhile" should "map emptyList into empty List" in {
    takeWhile(List[Int]())(_ > 0) should be(List[Int]())
  }

  it should "return first n elements of the List" in {
    takeWhile(List(1, 2, 3, 4, 5))(_ < 4) should be(List(1, 2, 3))
  }

  it should "return empty List when predicate is false for the first element" in {
    takeWhile(List(1, 2, 3, 4, 5))(_ < 0) should be(List[Int]())
  }

  it should "return full List when predicate is true for each element" in {
    takeWhile(List(1, 2, 3, 4, 5))(_ < 10) should be(List(1, 2, 3, 4, 5))
  }

  "forall" should "be true for empty List" in {
    forall(List[Int]())(_ < 10) should be (true)
  }

  it should "be true if the predicate evaluates to true for each item" in {
    forall(List(1, 2, 3, 4, 5))(_ < 10) should be (true)
  }

  it should "be false if the predicate evaluates to true for any item" in {
    forall(List(1, 2, 3, 4, 5))(_ != 10) should be (true)
  }

  "exists" should "be false for empty List" in {
    exists(List[Int]())(_ < 10) should be (false)
  }

  it should "be true if the predicate evaluates to true for any item" in {
    exists(List(1, 2, 3, 4, 5))(_ % 3 == 0) should be (true)
  }

  it should "be false if the predicate evaluates to false for each item" in {
    exists(List(1, 2, 3, 4, 5))(_ == 10) should be (false)
  }

  "scanLeft" should "return empty List for empty List" in {
    scanLeft(List[Int](), 0)(_ + _) should be (List[Int]())
  }

  it should "accumulate intermediate values" in {
    scanLeft(List(1, 2, 3, 4, 5), 0)(_ + _) should be (List(1, 3, 6, 10, 15))
  }

  "scanRight" should "return empty List for empty List" in {
    scanRight(List[Int](), 0)(_ + _) should be (List())
  }

  it should "accumulate intermediate values" in {
    scanRight(List(1, 2, 3, 4, 5), 0)(_ + _) should be (List(5, 9, 12, 14, 15))
  }

  it should "accumulate intermediate values 2" in {
    scanRight(List(1, 2, 3, 4, 5), 0)(_ - _) should be (List(5, -1, 4, -2, 3))
  }

  "hasSubsequence" should "be false when input list in empty" in {
    hasSubsequence(List[Int](), List(2)) should be(false)
  }

  it should "be false when main List doesn't contain sub List" in {
    hasSubsequence(List[Int](1, 3, 4), List(2)) should be(false)
  }

  it should "be true when main and sub List are the same singleton" in {
    hasSubsequence(List[Int](2), List(2)) should be(true)
  }

  it should "be true when main List contains sub (singleton) List" in {
    hasSubsequence(List[Int](1, 2, 3), List(2)) should be(true)
  }

  it should "be true when main List contains sub (multi-item) List" in {
    hasSubsequence(List[Int](1, 2, 3, 4, 5, 6), List(2, 3, 4)) should be(true)
  }

  it should "be true when main List starts with sub (multi-item) List" in {
    hasSubsequence(List[Int](1, 2, 3, 4, 5, 6), List(1, 2, 3, 4)) should be(true)
  }

  it should "be true when main List ends with with sub (multi-item) List" in {
    hasSubsequence(List[Int](1, 2, 3, 4, 5, 6), List(5, 6)) should be(true)
  }

  it should "be true when both Lists are empty" in {
    hasSubsequence(List[Int](), List[Int]()) should be (true)
  }

  it should "be true when sub List is empty" in {
    hasSubsequence(List[Int](1, 2, 3), List[Int]()) should be (true)
  }
}
