object FirstApp extends  App {
  println("Hello, World!")
  val o = new FirstClass()
  println(o.foobar(19, "xx"))
  println(o foobar 19)
  println(o foobar)
  println(o.foobar())
  val f = new First
  val res = f second() third() fourth() fifth()
}

class FirstClass {
  def foobar(x: Int) : Int = {
    2 * x
  }
  def foobar(x: Int, y: String) = {
    y + x.toString
  }
  def foobar() = 11
}

class First {
  def second() = new Second()
}

class Second {
  def third() = new Third()
}

class Third {
  def fourth() = new Fourth()
}

class Fourth {
  def fifth() = "Hello, World!"
}