package org.amicofragile.learning.scala.gameoflife

sealed trait Cell {
  def render : String
}

case class Alive() extends Cell {
  def render = "O"
}

case class Dead() extends Cell {
  def render = "-"
}
