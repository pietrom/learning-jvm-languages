package org.amicofragile.learning.scala.fp

import scala.annotation.tailrec

object Chapter02_01 {
  def fibonacci(n: Int) : Int = {
    if(n == 0 || n == 1) n else {
      @tailrec
      def loop(index: Int, prev: Int, prev2: Int) : Int = {
        if(index == n) prev + prev2 else loop(index + 1, prev + prev2, prev)
      }

      loop(2, 1, 0)
    }
  }

  def main(args: Array[String]): Unit = {
    for(i <- 1 to 10) {
      println(fibonacci(i))
    }
  }
}

object Chapter02_02 {
  def isSorted[A](as: Array[A], comparer: (A, A) => Boolean) : Boolean = {
    @tailrec
    def loop(index: Int): Boolean = {
      if(index >= as.length - 1) return true
      if(!comparer(as(index), as(index + 1))) false else loop(index + 1)
    }
    loop(0)
  }

  def main(args: Array[String]): Unit = {
    println(isSorted[Int](Array(), _ < _))
    println(isSorted[Int](Array(1, 3, 5, 7, 9, 11, 13), _ < _))
    println(isSorted[String](Array("aa", "cc", "vv", "bb", "zz"), _ < _))
    println(isSorted[String](Array("aa", "cc", "vv", "bb", "zz"), _ > _))
    println(isSorted[String](Array("aa", "cc", "vv", "yy", "zz"), _ < _))
  }
}

object Chapter02_03 {
  def partial1[A, B, C](a: A, f: (A, B) => C) : B => C = (b: B) => f(a, b)

  def curry[A, B, C](f: (A, B) => C) : A => (B => C) = (a: A) => (b: B) => f(a, b)

  def main(args: Array[String]): Unit = {
    val adder : (Int, Int) => Int = _ + _
    val add2 = partial1(2, adder)
    println(add2(11))
    println(add2(17))
    println(partial1(10, adder)(10))

    println(curry(adder)(2)(11))
    println(curry(adder)(2)(17))
    println(curry(adder)(10)(10))
  }
}

object Chapter02_04 {
  def uncurry[A, B, C](f: A => B => C) : (A, B) => C = (a: A, b: B) => f(a)(b)

  def main(args: Array[String]): Unit = {
    val partialAdder : Int => (Int => Int) = (x: Int) => (y: Int) => x + y
    val adder = uncurry(partialAdder)
    println(adder)
    println(adder(11, 19))
  }
}

object Chapter02_05 {
  def compose[A, B, C](f: B => C, g: A => B) : A => C = (a: A) => f(g(a))

  def main(args: Array[String]): Unit = {
    val double : Int => Int = (x: Int) => 2 * x
    val format : Int => String = (x: Int) => f"Double is $x%3d"
    val formatDouble = compose(format, double)
    println(formatDouble(11))
    println(formatDouble(19))
  }
}
