package org.amicofragile.learning.scala

object ImplicitTypeConversion extends App {
  implicit class IntExt(val value: Int) {
    def isOdd() = value % 2 != 0

    def isEven() = value % 2 == 0
  }

  implicit final class MyMapItemAssoc[A](key: A) {
    def ---->[B](value: B) = scala.Tuple2[A, B](key, value)
  }

  implicit def intToString(value: Int): String = value.toString()

  def printText(text: String) = println(s"We have a text: ${text}")

  println(s"10 isOdd? ${10.isOdd()}")
  println(s"10 isEven? ${10.isEven()}")

  printText("Hello")
  printText(19)

  val theMap = Map(1 ----> "Uno", 2 ----> "Due", 3 ----> "Tre")

  println(theMap)
}