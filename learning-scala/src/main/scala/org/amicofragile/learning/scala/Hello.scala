package org.amicofragile.learning.scala

class Hello {
  def sayHello() = "Hello, World!"

}

object Hello {
  def apply() = new Hello()
}
