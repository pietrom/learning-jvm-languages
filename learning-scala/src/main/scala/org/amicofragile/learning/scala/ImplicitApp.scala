package org.amicofragile.learning.scala

import java.time.ZonedDateTime

object ImplicitApp {
  def main(args: Array[String]): Unit = {
    implicit val clock = new SystemClock
    implicit val message = "Hi"
    val greeter = new Greeter()
    println(new Greeter()(clock, message).sayHello("Pietro"))
    println(new Greeter(clock).sayHello("Pietro"))
  }
}

trait Clock {
  def now() : ZonedDateTime
}

class SystemClock extends Clock {
  def now() = ZonedDateTime.now()
}

object Helper {
  def currentTime()(implicit clock: Clock): String = {
    return "[at " + clock.now().toOffsetDateTime + "]"
  }
}

class Greeter(implicit private val clock: Clock, implicit val msg: String) {
  def this(clock: Clock) = {
    this()(clock, "Hello")
  }
  def sayHello(to: String) = msg + ", " + to + Helper.currentTime()
}