package org.amicofragile.learning.scala

object Ext {
  implicit class IntExtensionWithFoo(val value: MyInt) {
    def foo() : String = s"FOO ${value.value}"
  }

  implicit class IntExtensionWithBar(val value: MyInt) {
    def bar() : String = s"BAR ${value.value}"
  }

  implicit class IntExtensionForMoney(val value: Int) {
    def apply(currency: String) : MMoney = MMoney(value, currency)
  }
}

case class MyInt(val value: Int) {
  def foo() : String = s"MyInt.foo $value"
}

case class MMoney(val amount: Int, currency: String)

object ImplicitFoo extends App {
  import Ext._

  val x = MyInt(123)
  println(x.foo())
  println(x.bar())

  val m = 100("EUR")
  println(m)
}
