package org.amicofragile.learning.scala.gameoflife

class Grid(val cells: Array[Array[Cell]]) {
  def render() : String = {
    return cells.foldLeft("")(
      (acc: String, curr: Array[Cell]) => {
        acc + curr.foldLeft("")((rowAcc: String, rowCurr:Cell) => rowAcc + rowCurr.render) + "\n"
      })
  }

  def evolve() : Grid = this
}
