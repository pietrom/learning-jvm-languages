package org.amicofragile.learning.scala

object PatternMatching extends App {
  def applyPatternMatching(input : Parent) = input match {
    case Child1(17, _) => println("17")
    case Child1(a, b) => println("Child1: " + a + ", " + b)
    case Child2(v) => println(s"Child2: $v")
  }

  applyPatternMatching(new Child1(11, "19"))
  applyPatternMatching(new Child1(17, "22"))
  applyPatternMatching(new Child2("xyz"))
}

sealed trait Parent

case class Child1(val x: Int, val y: String) extends Parent

case class Child2(val value: String) extends Parent