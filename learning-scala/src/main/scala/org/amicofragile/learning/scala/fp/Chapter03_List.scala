package org.amicofragile.learning.scala.fp

import scala.annotation.tailrec

class Pippo {}

object Chapter03_List {

  sealed trait List[+A]

  object Nil extends List[Nothing] {
    override def toString: String = "Nil"
  }

  case class Cons[+A](head: A, tail: List[A]) extends List[A]

  object List {
    def apply[A](xs: A*): List[A] =
      if (xs.isEmpty) Nil
      else Cons(xs.head, apply(xs.tail: _*))

    def sum(ints: List[Int]): Int = ints match {
      case Nil => 0
      case Cons(i, tail) => i + sum(tail)
    }

    def sum_tailrec(ints: List[Int]): Int = {
      @tailrec
      def loop(acc: Int, values: List[Int]): Int = {
        values match {
          case Nil => acc
          case Cons(h, t) => loop(acc + h, t)
        }
      }

      loop(0, ints)
    }

    def prod(ints: List[Int]): Int = ints match {
      case Nil => 1
      case Cons(0, _) => 0
      case Cons(h, t) => h * prod(t)
    }

    def head[A](list: List[A]): Option[A] = list match {
      case Nil => None
      case Cons(h, _) => Some(h)
    }

    def tail[A](list: List[A]): List[A] = list match {
      case Nil => Nil
      case Cons(_, t) => t
    }

    @tailrec
    def drop[A](list: List[A], n: Int): List[A] = {
      if (n == 0)
        list
      else
        list match {
          case Nil => Nil
          case Cons(_, t) => drop(t, n - 1)
        }
    }

    @tailrec
    def dropWhile[A](list: List[A])(f: A => Boolean): List[A] = list match {
      case Nil => Nil
      case Cons(h, t) => if (!f(h)) list else dropWhile(t)(f)
    }

    def foldRight[A, B](list: List[A], z: B)(f: (A, B) => B): B = list match {
      case Nil => z
      case Cons(h, t) => f(h, foldRight(t, z)(f))
    }

    def foldRight_fl[A, B](list: List[A], z: B)(f: (A, B) => B): B = foldLeft(reverse_fl(list), z)((b: B, a: A) => f(a, b))

    @tailrec
    def foldLeft[A, B](list: List[A], z: B)(f: (B, A) => B): B = list match {
      case Nil => z
      case Cons(h, t) => foldLeft(t, f(z, h))(f)
    }

    def foldLeft_fr[A, B](list: List[A], z: B)(f: (B, A) => B): B = foldRight(reverse_fr(list), z)((a: A, b: B) => f(b, a))

    def setHead[A](list: List[A])(newHead: A): List[A] = list match {
      case Nil => Cons(newHead, Nil)
      case Cons(h, t) => Cons(newHead, t)
    }

    def append[A](l1: List[A], l2: List[A]): List[A] = l1 match {
      case Nil => l2
      case Cons(h, t) => Cons(h, append(t, l2))
    }

    def length[A](list: List[A]): Int = {
      @tailrec
      def loop(acc: Int, as: List[A]): Int = as match {
        case Nil => acc
        case Cons(_, t) => loop(1 + acc, t)
      }

      loop(0, list)
    }

    def length_fr[A](list: List[A]): Int = foldRight(list, 0)((_, acc) => acc + 1)

    def length_fl[A](list: List[A]): Int = foldLeft(list, 0)((acc, _) => acc + 1)

    def addFirst[A](list: List[A], newHead: A) : List[A] = Cons(newHead, list)

    def reverse[A](list: List[A]) : List[A] = list match {
      case Nil => Nil
      case Cons(h, t) => append(reverse(t), List(h))
    }

    def reverse_fl[A](list: List[A]) : List[A] = foldLeft(list, List() : List[A])((acc, curr) => Cons(curr, acc))

    def reverse_fr[A](list: List[A]) : List[A] = foldRight(list, List() : List[A])((curr, acc) => append(acc, List(curr)))

    def flatten[A](list: List[List[A]]) : List[A] =
      foldLeft(list, List[A]())((acc, curr) => append(acc, curr))

    def map[A, B](list: List[A])(f: A => B) : List[B] = list match {
      case Nil => Nil
      case Cons(h, t) => Cons(f(h), map(t)(f))
    }

    def map_fl[A, B](list: List[A])(f: A => B) : List[B] = foldLeft(list, List[B]())((acc, curr) => append(acc, List(f(curr))))

    def flatMap[A, B](list: List[A])(f: A => List[B]) : List[B] = foldLeft(list, List[B]())((acc, curr) => append(acc, f(curr)))

    def filter[A](list: List[A])(p: A => Boolean) : List[A] = list match {
      case Nil => Nil
      case Cons(h, t) => if(p(h)) Cons(h, filter(t)(p)) else filter(t)(p)
    }

    def filter_fm[A](list: List[A])(p: A => Boolean) : List[A] = flatMap(list)((i) => if(p(i)) List(i) else List[A]())

    def mapZipped[A, B, C](l1: List[A], l2: List[B])(f: (A, B) => C) : List[C] = map(zip(l1, l2))((t) => f(t._1, t._2))

    def zip[A, B](l1: List[A], l2: List[B]) : List[(A, B)] = {
      l1 match {
        case Nil => Nil
        case Cons(h1, t1) => l2 match {
          case Nil => Nil
          case Cons(h2, t2) => Cons((h1, h2), zip(t1, t2))
        }
      }
    }

    def take[A](list: List[A], n: Int) : List[A] = list match {
      case Nil => Nil
      case Cons(h, t) => if(n > 0) Cons(h, take(t, n - 1)) else Nil
    }

    def takeWhile[A](list: List[A])(p: A => Boolean) : List[A] = list match {
      case Nil => Nil
      case Cons(h, t) => if(p(h)) Cons(h, takeWhile(t)(p)) else Nil
    }

    @tailrec
    def forall[A](list: List[A])(p: A => Boolean) : Boolean = list match {
      case Nil => true
      case Cons(h, t) => if(!p(h)) false else forall(t)(p)
    }

    @tailrec
    def exists[A](list: List[A])(p: A => Boolean) : Boolean = list match {
      case Nil => false
      case Cons(h, t) => if(p(h)) true else exists(t)(p)
    }

    def scanLeft[A, B](list: List[A], z: B)(f: (B, A) => B) : List[B] = list match {
      case Nil => Nil
      case Cons(h, t) =>
        val intermediate = f(z, h)
        append(List(intermediate), scanLeft(t, intermediate)(f))
    }

    def scanRight[A, B](list: List[A], z: B)(f: (A, B) => B) : List[B] = {
      def loop(as: List[A], z: B) : (B, List[B]) = {
        as match {
          case Nil => (z, Nil)
          case Cons(h, t) =>
            val sub = loop(t, z)
            val temp = f(h, sub._1)
            (temp, append(sub._2, List(temp)))
        }
      }
      loop(list, z)._2
    }

    def last[A](list: List[A]) : Option[A] = list match {
      case Nil => None
      case Cons(h, Nil) => Some(h)
      case Cons(_, t) => last(t)
    }

    def hasSubsequence[A](list: List[A], sub: List[A]) : Boolean = sub match {
      case Nil => true
      case Cons(h1, t1) => list match {
        case Nil => false
        case Cons(h2, t2) => (h2 == h1 && startsWith(t2, t1)) || hasSubsequence(t2, sub)
      }
    }

    def startsWith[A](list: List[A], sub: List[A]) : Boolean = sub match {
      case Nil => true
      case Cons(h, t) => list match {
        case Nil => false
        case Cons(hl, tl) => hl == h && startsWith(tl, t)
      }
    }
  }
}
