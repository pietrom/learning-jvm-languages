package org.amicofragile.learning.scala

object LengthUnitiesConversionHelper {
  implicit class LengthUnitiesConversion(x: Double) {
    def m() : Distance = Distance(x)
    def km() : Distance = Distance(1000 * x)
    def yd() : Distance = Distance(0.9144 * x)
  }
}
