package org.amicofragile.learning.scala

import org.amicofragile.learning.scala.Cart.{CartField, CartPredicate, CountryToTaxCalculation, TaxCalculator}
import org.amicofragile.learning.scala.Countries.Canada

sealed trait Country

object Countries {
  object Canada extends Country
  object Us extends Country
  object Uk extends Country
  object Sf extends Country
  object Israel extends Country
}

case class Cart(total: BigDecimal, shipping: BigDecimal, discount: BigDecimal, country: Country)

class CartCombinator(val cartField: CartField) {
  def and(comb: CartCombinator) : CartCombinator  =
    new CartCombinator((c: Cart) => cartField(c) + comb.cartField(c))

  def &(comb: CartCombinator) : CartCombinator  = and(comb)

  def If(cond: CartPredicate) : CartCombinator  =
    new CartCombinator((c: Cart) => if(cond(c)) cartField(c) else 0)

  def > (value: BigDecimal) : CartPredicate =
    (c: Cart) => cartField(c) > value
}

object Cart {
  val shipping = new CartCombinator((c: Cart) => c.shipping)
  val discount = new CartCombinator((c: Cart) => c.discount)

  type CartField = Cart => BigDecimal
  type CartPredicate = Cart => Boolean
  type TaxCalculator = Cart => BigDecimal
  type CountryToTaxCalculation = (Country, TaxCalculator)
}

case class CountryContainer(val country: Country) {
  def take(tax: BigDecimal): TaxAndCountry = {
    TaxAndCountry(country, tax)
  }
}

object CountryContainer {
  def For(country: Country) = CountryContainer(country)
}

case class TaxAndCountry(country: Country, tax: BigDecimal) {
  def ignore(comb: CartCombinator) : CountryToTaxCalculation =
      (country, (cart: Cart) => (cart.total - comb.cartField(cart)) * tax)

  def addTouristPrice(f: Cart => BigDecimal) = addCustomValue(f)

  private def addCustomValue(cartField: CartField) : CountryToTaxCalculation =
    (country, (c: Cart) => (c.total + cartField(c)) * tax)
}

object TaxAndCountry {
  implicit def taxAndCountryToTaxCalculation(tacc: TaxAndCountry) : CountryToTaxCalculation = {
    (tacc.country, (cart:Cart) => cart.total * tacc.tax)
  }
}

object Taxes extends App {
  import org.amicofragile.learning.scala.Countries._
  import CountryContainer.For
  import Cart.shipping
  import Cart.discount

  implicit class TaxRuleSeqExt(taxRules: Seq[CountryToTaxCalculation]) {
    def findRuleFor(cart: Cart) =
      taxRules.find(_._1 == cart.country).map(_._2)
  }

  val ca: TaxAndCountry = For(Canada) take 0.15
  val uk: (Country, TaxCalculator) = For(Uk) take 0.12 ignore shipping
  val us: (Country, TaxCalculator) = For(Us) take 0.07 ignore (shipping and discount)
  val us2: (Country, TaxCalculator) = For(Us) take 0.07 ignore shipping & discount
  val sf: (Country, TaxCalculator) = For(Sf) take 0.05 ignore discount.If(shipping > 4)
  val sf2: (Country, TaxCalculator) = For(Sf) take 0.05 ignore (discount If shipping > 4)
  def israel: (Country, TaxCalculator) = For(Israel) take 0.25 addTouristPrice { cart => cart.total * 0.2}

  val calculator : TaxCalculator = ca._2

  println(ca._2(Cart(1000, 100, 20, Countries.Canada)))
  println(uk._2(Cart(1000, 100, 20, Countries.Uk)))
  println(us._2(Cart(1000, 100, 20, Countries.Us)))
  println(us2._2(Cart(1000, 100, 20, Countries.Us)))

  println(sf._2(Cart(1000, 100, 20, Countries.Sf)))
  println(sf._2(Cart(1000, 3, 20, Countries.Sf)))

  println(sf2._2(Cart(1000, 100, 20, Countries.Sf)))
  println(sf2._2(Cart(1000, 3, 20, Countries.Sf)))

  println(israel._2(Cart(1000, 100, 20, Countries.Israel)))

  val taxRules: Seq[CountryToTaxCalculation] = Seq(
    ca, uk, us, sf, israel
  )
  println("===")
  private val cart = Cart(1000, 100, 20, Uk)
  val taxRule: Option[TaxCalculator] = taxRules findRuleFor cart
  println(taxRule.map(_(cart)))
}
