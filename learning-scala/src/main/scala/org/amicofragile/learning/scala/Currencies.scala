package org.amicofragile.learning.scala

case class Money(val avlue: Double, val curr: Currency)

object TheEuro {
  def apply(value: Double) = Money(value, Euro)
}

object TheUsd {
  def apply(value: Double) = Money(value, Usd)
}

sealed trait Currency
case object Euro extends Currency
case object Usd extends Currency

object Currencies {
  implicit class IntExt(val value : Int) {
    def apply(curr: Currency) : Money = Money(value, curr)
  }
}

import Currencies.IntExt

object Run extends App {
  println(s"${TheUsd(10)} ${TheEuro(20)}")

  println(s"${10(Usd)} ${20(Euro)}")
}
