package org.amicofragile.learning.scala

case class Distance(val value: Double) {
  def +(other: Distance) = Distance(this.value + other.value)
}
